# tf-helm-image

Commits to this project trigger a pipline that builds and publishes a Docker image that includes the following major packages:

 - terraform
 - kubectl
 - helm
 - gcloud
 - jq

The image (to be pulled from the Gitlab Container Registry associated to this project) is used to provision and configure Kubernetes clusters. 
An example usage in a pipeline: https://gitlab.com/azizos/bobbapp/env/infra