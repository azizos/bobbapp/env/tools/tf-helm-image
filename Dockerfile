FROM alpine:3.12.0

ENV TERRAFORM_VERSION 0.12.23
ENV HELM_VERSION 3.2.4
ENV KUBECTL_VERSION 1.18.0
ENV GCLOUD_VERSION 299.0.0
ENV FLUX_VERSION 1.18.0

## Install utilities
RUN apk add --no-cache --update ca-certificates curl bash jq python3 py3-setuptools

## Install terraform
RUN curl https://releases.hashicorp.com/terraform/${TERRAFORM_VERSION}/terraform_${TERRAFORM_VERSION}_linux_amd64.zip -o terraform_${TERRAFORM_VERSION}_linux_amd64.zip
RUN unzip terraform_${TERRAFORM_VERSION}_linux_amd64.zip
RUN rm terraform_${TERRAFORM_VERSION}_linux_amd64.zip
RUN mv ./terraform /usr/local/bin

## Install kubectl
RUN curl -LO https://storage.googleapis.com/kubernetes-release/release/v${KUBECTL_VERSION}/bin/linux/amd64/kubectl
RUN chmod +x ./kubectl
RUN mv ./kubectl /usr/local/bin

## Install helm
RUN curl -L https://get.helm.sh/helm-v${HELM_VERSION}-linux-amd64.tar.gz | tar xvz
RUN chmod +x linux-amd64/helm
RUN mv ./linux-amd64/helm /usr/local/bin

## Install fluxctl

RUN curl -L https://github.com/fluxcd/flux/releases/download/${FLUX_VERSION}/fluxctl_linux_amd64 -o /usr/local/bin/fluxctl
RUN chmod +x /usr/local/bin/fluxctl

## Install gcloud to authenticate to the cluster
RUN curl https://dl.google.com/dl/cloudsdk/channels/rapid/downloads/google-cloud-sdk-${GCLOUD_VERSION}-linux-x86_64.tar.gz >/tmp/google-cloud-sdk.tar.gz
RUN mkdir -p /usr/local/gcloud && tar -C /usr/local/gcloud -xvf /tmp/google-cloud-sdk.tar.gz && /usr/local/gcloud/google-cloud-sdk/install.sh
ENV PATH $PATH:/usr/local/gcloud/google-cloud-sdk/bin
